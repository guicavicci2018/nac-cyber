import random
import csv
import secrets

def definePrimo(n, k=10):
    if n == 2:
        return True
    if n % 2 == 0:
        return False
    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2
    for _ in range(k):
        a = random.randrange(2, n - 1)
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True

def geraNumero():
    numero = secrets.randbits(100)
    while definePrimo(numero) is False:
        numero = secrets.randbits(100)
        #print (numero)
    return (numero)

#def primoRel(phi):
#    x = 0
#    gcd = 0
#    while gcd != 1:
#        x = geraNumero(1, phi-1)
#        gcd = euclides(x,phi)
#    print("primorel")
#    return x

def rsa():
    p = geraNumero()
    q = geraNumero()
    n = p*q
    phi = (p-1)*(q-1)
    e = random.randrange(1, phi)
    #print ("ESSE E O E")
    #print (e)
    x,d,y = extEuclides(e, phi)
    while d < 0:
        x,d,y = extEuclides(e,phi)
    p = e
    s = d
    #print ("O VALOR DE P E ")
    #print (p)
    return p,s,n



def euclides(a,b):
    while b != 0:
        a,b = b, a%b
    #print("euclides")
    return a

def extEuclides(a,b):
    if b == 0:
    	#print ("if do exteuclides")
        return (a,1,0)
    d1,x1,y1 = extEuclides(b, a%b)
    d,x,y = d1, y1, x1 - (a//b)*y1
    #print ("VALOR DE B: ")
    #print (b)
    return d,x,y

def encriptar(mensagem, p, n):
    msgEncriptada = pow(mensagem, p, n)	
    return msgEncriptada

def decriptar(mensagem, secreta, n):
    msgDecriptada = pow(mensagem, secreta, n)

    for palavra in mensagem.split():
        for i in range(0, len(palavra)):
            arrayLetras.extend(palavra[i])



    return msgDecriptada


def buscarNumero(letra):

    with open('tabela.csv', newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter='\n', quotechar='\n')
        for row in spamreader:
            for texto in row:
                if letra in texto:
                    return texto[2:]

def retornaMensagem(mensagem):
	arrayLetras = []

	for palavra in mensagem.split():
		for i in range(0, len(palavra)):
			arrayLetras.extend(palavra[i])

	return arrayLetras

arrayNumeros = []
letras = retornaMensagem("teste")
#letras = retornaMensagem("the information security is of great importance to ensure the privacy of communications")
for l in letras:
	arrayNumeros.extend(buscarNumero(l))
numeros = ''
numeros = int(numeros.join(arrayNumeros))

mensagem = numeros
print(mensagem)

chPublic, chPriv, n = rsa()
print(chPublic, chPriv, n)

#msgEncriptada = encriptar(mensagem, chPublic, n)
#print(msgEncriptada)

msgEncriptada = encriptar(mensagem, chPublic, n)
print(msgEncriptada)

msgDecriptada = decriptar(msgEncriptada, chPriv, n)
print(msgDecriptada)

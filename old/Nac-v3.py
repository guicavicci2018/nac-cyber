import random
import math
import secrets

def miller_rabin(n, k=10):
    if n == 2:
        return True

    if n % 2 == 0:
        return False

    r, s = 0, n - 1
    while s % 2 == 0:
        r += 1
        s //= 2
    for _ in range(k):
        a = random.randrange(2, n - 1)
        x = pow(a, s, n)
        if x == 1 or x == n - 1:
            continue
        for _ in range(r - 1):
            x = pow(x, 2, n)
            if x == n - 1:
                break
        else:
            return False
    return True


def definePrimo(numero):
    primoRaiz = int(math.sqrt(numero))
    for i in range(2, primoRaiz):
        if numero%i == 0:
            #print (numero)
            return False
    return True


def geraNumero():
    n = 100
    numero = secrets.randbits(n)
    while miller_rabin(numero) is False:
        numero = secrets.randbits(n)
    return numero



def primoRel(phi):
    x = 0
    gcd = 0
    while gcd != 1:
        x = geraNumero(1, phi)
        gcd = euclides(x,phi)
    print("primorel")
    return x

def euclides(a,b):
    while b != 0:
        a,b = b, a%b
    return a

def extEuclides(a,b):
    if b == 0:
        return (a,1,0)
    print ("Looping do B dentro do extEuclides")
    d1,x1,y1 = extEuclides(b, a%b)
    d,x,y = d1, y1, x1 - (a//b)*y1

    return d,x,y

def rsa():
    p,q = geraNumero(), geraNumero()
    print ("Valor de P: ")
    print (p)

    print ("Valor de Q: ")
    print (q)

    n = p * q

    print("Valor de N: ")
    print(n)

    phi = (p-1)*(q-1)

    print ("Totiente (phi): ")
    print (phi)

    e = random.randrange(1, phi)

    while euclides(e,phi) !=1:
        e = random.randrange(1, phi)
    print ("Valor de E:")
    print (e)
    x,d,y = extEuclides(e, phi)
    while d < 0:
        x,d,y = extEuclides(e,phi)
    chPublic = (e,n)
    chPriv = (d,n)
    return chPublic,chPriv

chPublic, chPriv = rsa()
print ("Chave Publica: ", chPublic)

print ("Chave Privada: ", chPriv)

def encriptar(chPublic, mensagem):
    e, n = chPublic
    return [pow(ord(char), e, n) for char in mensagem]

def decriptar(chPriv, msgEncriptada):
    d, n = chPriv
    return ''.join([chr(pow(char, d, n)) for char in msgEncriptada])

mensagem = ("Israel")

msgEncriptada = encriptar(chPublic, mensagem)
print ("Mensagem encriptada:", msgEncriptada)


msgDecriptada = decriptar(chPriv, msgEncriptada)
print ("Mensagem decriptada:", msgDecriptada)
